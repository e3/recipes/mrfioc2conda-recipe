#!/bin/bash


LIBVERSION=${PKG_VERSION}

# This is the directory that the ESS-specific scripts are extracted into
export ESS_MRFIOC2_DIR=e3wrap
echo "INFO: Build prefix"
echo "${BUILD_PREFIX}"
echo "${EPICS_MODULES}"
echo "${LIBVERSION}"
echo "${PRJ}"

# Clean between variants builds
make -f Makefile.E3 clean
echo "INFO: Clean returns $?"

#Copy a new the substitution file patch 
#patch -d e3wrap -p1 <patch/evr-pcie-300dc.substitutions.patch.patch
cp patch/evr-pcie-300dc.substitutions.patch e3wrap/patch/evr-pcie-300dc.substitutions.patch

for i in e3wrap/patch/*.patch; 
do patch -p1 < $i; 
done

patch -d e3wrap -p1  <patch/conf.yaml.patch

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
echo "INFO: Build returns $?"
# copy over ess specific substitutions
# not sure why this isn't done in the Makefile but in configure/module/RULES_MODULE...
#install -m 644 ${ESS_MRFIOC2_DIR}/template/evg-*-ess.substitutions  evgMrmApp/Db/
#install -m 644 ${ESS_MRFIOC2_DIR}/template/evm-*-ess.substitutions  evgMrmApp/Db/
#install -m 644 ${ESS_MRFIOC2_DIR}/template/evr-*-ess.substitutions  evrMrmApp/Db/
#install -m 644 ${ESS_MRFIOC2_DIR}/template/evr-databuffer-*.template  evrMrmApp/Db/

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
echo "INFO: Database returns $?"

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install
echo "INFO: Install returns $?"
#echo "INFO: Build prefix"
# ls ${BUILD_PREFIX}
# echo "INFO files"
# ls ${BUILD_PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/

# install the ess iocsh scripts into the directory
# install -m 644 ess-iocsh/*.iocsh  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/
